def colors(c, opts = {}):
    palette = {
        'status_bg': '#211d30',
        'background_secondary': '#1F2133', 
        'completion_secondary': '#1F2133', 

        'background_attention': '#181920',
        'background_insert': '#a6b2cc',

        'border_color': '#282a36',
        'current_line_bg': '#44475a',

        'selection_bg': '#DDDFFF',

        'foreground_primary': '#CCCEE0',
        'foreground_normal': '#DDDFFF',
        'foreground_secondary': '#0b121f',

        'foreground_attention': '#000000',
        'text_comment': '#6272a4',
        'color_cyan': '#8be9fd',

        'tab_selected_bg': '#DDDFFF',
        'foreground_alt1': '#000000',

        'color_green': '#73D216',
        'color_orange': '#FCBA9C',

        'status_fg': '#DEE7F7',
        'color_match': '#50FA7B',

        'color_purple': '#bd93f9',
        'color_kh': '#f1fa8c',
        'color_red': '#ff5555',
        'color_yellow': '#f1fa8c',
    }

    # Colors for the completion widget 
    c.colors.completion.category.bg = palette['status_bg']
    c.colors.completion.category.fg = palette['foreground_primary']

    c.colors.completion.category.border.bottom = palette['border_color']
    c.colors.completion.category.border.top = palette['border_color']

    c.colors.completion.even.bg = palette['status_bg']
    c.colors.completion.odd.bg = palette['completion_secondary']

    c.colors.completion.fg = palette['foreground_normal']

    c.colors.completion.item.selected.bg = palette['selection_bg']
    c.colors.completion.item.selected.border.bottom = palette['selection_bg']
    c.colors.completion.item.selected.border.top = palette['selection_bg']

    c.colors.completion.item.selected.fg = palette['foreground_secondary']
    c.colors.completion.match.fg = palette['color_match']

    c.colors.completion.scrollbar.bg = palette['status_bg']
    c.colors.completion.scrollbar.fg = palette['foreground_primary']

    ## Background color for the download bar.
    c.colors.downloads.bar.bg = palette['status_bg']

    ## Background color for downloads with errors.
    c.colors.downloads.error.bg = palette['status_bg']

    ## Foreground color for downloads with errors.
    c.colors.downloads.error.fg = palette['color_red']

    ## Color gradient start for download.
    c.colors.downloads.start.bg = palette['color_yellow']
    c.colors.downloads.start.fg = "#000000"

    ## Color gradient stop for download.
    c.colors.downloads.stop.bg = palette['color_orange']
    c.colors.downloads.stop.fg = "#000000"

    ## Color gradient interpolation system for download backgrounds.
    c.colors.downloads.system.bg = 'rgb'

    ## Background color for hints.
    c.colors.hints.bg = palette['status_bg']

    ## Font color for hints.
    c.colors.hints.fg = palette['color_kh']

    ## Border color for hints.
    c.hints.border = '1px solid ' + palette['background_secondary']

    ## Font color for the matched part of hints.
    c.colors.hints.match.fg = palette['foreground_secondary']

    ## Background color of the keyhint widget.
    c.colors.keyhint.bg = palette['status_bg']

    ## Text color for the keyhint widget.
    c.colors.keyhint.fg = palette['color_yellow']

    ## Highlight color for keys to complete the current keychain.
    c.colors.keyhint.suffix.fg = palette['color_kh']

    ## Background color of an error message.
    c.colors.messages.error.bg = palette['status_bg']

    ## Border color of an error message.
    c.colors.messages.error.border = palette['background_secondary']

    ## Foreground color of an error message.
    c.colors.messages.error.fg = palette['color_red']

    ## Background color of an info message.
    c.colors.messages.info.bg = palette['status_bg']

    ## Border color of an info message.
    c.colors.messages.info.border = palette['color_yellow']

    ## Foreground color of an info message.
    c.colors.messages.info.fg = palette['color_yellow']

    ## Background color of a warning message.
    c.colors.messages.warning.bg = palette['status_bg']

    ## Border color of a warning message.
    c.colors.messages.warning.border = palette['background_secondary']

    ## Foreground color of a warning message.
    c.colors.messages.warning.fg = palette['color_red']

    ## Background color for prompts.
    c.colors.prompts.bg = palette['status_bg']

    ## Border color used around UI elements in prompts.
    c.colors.prompts.border = '1px solid ' + palette['background_secondary']

    ## Foreground color for prompts.
    c.colors.prompts.fg = palette['color_cyan']

    ## Background color for the selected item in filename prompts.
    c.colors.prompts.selected.bg = palette['selection_bg']

    ## Background color of the statusbar.
    c.colors.statusbar.normal.bg = palette['status_bg']

    ## Foreground color of the statusbar.
    c.colors.statusbar.normal.fg = palette['foreground_normal']

    ## Foreground color of the URL in the statusbar on error.
    c.colors.statusbar.url.error.fg = palette['color_red']

    ## Foreground color of the URL in the statusbar on successful load (HTTP).
    c.colors.statusbar.url.success.http.fg = palette['color_green']

    ## Foreground color of the URL in the statusbar on successful load (HTTPS).
    c.colors.statusbar.url.success.https.fg = palette['color_green']

    ## Foreground color of the URL in the statusbar when there's a warning.
    c.colors.statusbar.url.warn.fg = palette['color_yellow']


    ## Tabs indicator
    c.colors.tabs.indicator.error = palette['color_red']
    c.colors.tabs.indicator.start = palette['color_orange']
    c.colors.tabs.indicator.stop = palette['color_green']
    c.colors.tabs.indicator.system = 'rgb'

    ## Background color of the tab bar.
    c.colors.tabs.bar.bg = palette['status_bg']

    ## Background color of unselected tabs.
    c.colors.tabs.even.bg = palette['status_bg']
    c.colors.tabs.odd.bg = palette['status_bg']

    ## Foreground color of unselected tabs.
    c.colors.tabs.even.fg = palette['foreground_primary']
    c.colors.tabs.odd.fg = palette['foreground_primary']

    ## Background color of selected tabs.
    c.colors.tabs.selected.even.bg = palette['tab_selected_bg']
    c.colors.tabs.selected.odd.bg = palette['tab_selected_bg']

    ## Foreground color of selected tabs.
    c.colors.tabs.selected.even.fg = palette['foreground_alt1']
    c.colors.tabs.selected.odd.fg = palette['foreground_alt1']

    c.tabs.indicator.width = 5
    c.tabs.favicons.scale = 1.1

    ## Default font settings
    c.fonts.default_family = 'JetBrainsMonoNLNerdFontMono'
    c.fonts.default_size = '11pt'
